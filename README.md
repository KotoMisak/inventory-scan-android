# inventory-scan-android

![alt text](./app/src/main/ic_launcher-web-200.png "KoTiLogo")

[KoTiProject overview](http://kotopeky.cz/project)

## What is InventoryScanner for?

Inventory scanner is intended to use as QR scanner for items in property register.

## Why to use this QR?
This scanner is connected to local database which is synchronized with the dedicated server.
Transfer of scanned items is fully automatized.
User scan the item and immediately decide whether this is intended item (whether to add item to database or not).
Inventory scanner also offers simplified list of all items in register (with quick search functionality).

## Why android application?

Android application (in comparison with web application) offer effective data caching.
Android compared to web container offer direct usage of hardware (camera) as scanner.


## Notes
ZBar(QR) Scanner wrapped with automatic data processing.

This version is still rather skeleton with plenty of TODO.
The plan is improve and add these features:

- Directly display scanned data together with decision buttons.
- Improve UX in list of scanned data (add intuitive search, sort and display).
- Add possibility to export scanned data (to email as file or to server as backup).
- Replace mocked starting source with source from the server.
- ....


![Captured QR](./scr/scr_captured.png)

![Scanned items list](./scr/scr_list.png)