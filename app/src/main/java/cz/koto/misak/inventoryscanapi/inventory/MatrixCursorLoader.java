package cz.koto.misak.inventoryscanapi.inventory;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

public class MatrixCursorLoader extends CursorLoader {
    private final Cursor cursor;

    public MatrixCursorLoader(Context context, Cursor cursor) {
        super(context);
        this.cursor = cursor;
    }

    @Override
    protected Cursor onLoadInBackground() {
        return cursor;
    }
}