package cz.koto.misak.inventoryscanapi.api;

import java.util.List;

import retrofit.http.GET;

public interface KoTiNodeApi {

    @GET("/events")
    List<String> listEvents();
}
