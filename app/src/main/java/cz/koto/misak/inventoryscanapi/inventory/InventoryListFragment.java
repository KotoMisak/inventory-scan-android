package cz.koto.misak.inventoryscanapi.inventory;

import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.List;

import cz.koto.misak.inventoryscanapi.R;

public class InventoryListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView mList;

    // The callbacks through which we will interact with the LoaderManager.
    private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;

    // The adapter that binds our data to the ListView
    private SimpleCursorAdapter mAdapter;

    private static final int LOADER_ID = 1;

    public InventoryListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * assign variables, get Intent extras, and anything else that
         * doesn't involve the View hierarchy (i.e. non-graphical initialisations).
         */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        /**
         * assign your View variables and do any graphical initialisations
         */
        return inflater.inflate(R.layout.fragment_inventory_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /**
         * Mainly used for final initialisations (for example, modifying UI elements).
         */
        mList = (ListView) getView().findViewById(android.R.id.list);
        showData();
    }

    protected void showData() {
        Resources rsc = getResources();


        int[] viewIDs = new int[]{R.id.itemImgStatus, R.id.itemTxtStatus, R.id.itemTxt};
        String[] columns = new String[]{"itemImgStatus", "itemTxtStatus", "itemTxt"};
        mAdapter = //new SimpleCursorAdapter(getActivity().getBaseContext(), R.layout.inventory_list_item, cData, columns, to);
        new SimpleCursorAdapter(getActivity().getBaseContext(), R.layout.inventory_list_item,
                null, columns, viewIDs, Adapter.NO_SELECTION);
        mList.setAdapter(mAdapter);
        mCallbacks = this;

        LoaderManager lm = this.getLoaderManager();
        lm.initLoader(LOADER_ID, null, mCallbacks);
    }

    /**
     * ********************************************************************
     */
    public void mergeItem(String item){
        Log.i(getTag(), ">>" + item + "<<");

        // Merge your existing cursor with the matrixCursor you created.
        MergeCursor mergeCursor = new MergeCursor(new Cursor[] { InventoryCursorFactory.getCursor(getResources(),new InventoryItem(null,Boolean.TRUE,item)), mAdapter.getCursor() });
        mAdapter.swapCursor(mergeCursor);
    }

    /**
     * ********************************************************************
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Create a new CursorLoader with the following query parameters.
        return new MatrixCursorLoader(getActivity(),InventoryCursorFactory.getCursor(getResources()));
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // A switch-case is useful when dealing with multiple Loaders/IDs
        switch (loader.getId()) {
            case LOADER_ID:
                // The asynchronous load is complete and the data
                // is now available for use. Only now can we associate
                // the queried Cursor with the SimpleCursorAdapter.
                mAdapter.swapCursor(cursor);
                break;
        }
        // The listview now displays the queried data.
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // For whatever reason, the Loader's data is now unavailable.
        // Remove any references to the old data by replacing it with
        // a null Cursor.
        mAdapter.swapCursor(null);
    }



}
