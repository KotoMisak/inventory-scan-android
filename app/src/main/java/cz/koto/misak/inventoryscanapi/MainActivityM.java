package cz.koto.misak.inventoryscanapi;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.List;

import cz.koto.misak.inventoryscanapi.inventory.InventoryListFragment;
import cz.koto.misak.inventoryscanapi.scanner.ScannerFragment;

public class MainActivityM extends AppCompatActivity implements ScannerFragment.ScannResult{

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private int[] tabIcons = {
            R.drawable.ico_qr_32,
            R.drawable.ico_list_32
    };

    private static final int ZBAR_SCANNER_REQUEST = 0;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;
    private static final String TAG = "ZBarFragment";
    private GestureDetector gestureDetector;
    private ScannerFragment scannerFragment;
    private InventoryListFragment listFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_m);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        for (int i=0; i<tabLayout.getTabCount();i++){
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        Resources rsc = getResources();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        gestureDetector = new GestureDetector(this, new GestureListener());



        adapter.addFragment(scannerFragment = new ScannerFragment(), /*rsc.getString(R.string.action_scann)*/"");
        adapter.addFragment(listFragment = new InventoryListFragment(), /*rsc.getString(R.string.action_list)*/"");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    /**
     * *********************************************************
     */
    @Override
    public void sendResult(String text){
        listFragment.mergeItem(text);
    }
    /**
     * *********************************************************
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        scannerFragment.onKeyDown(keyCode, event);
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        //setLabel(getString(R.string.scan_label));

        // start scanning only after a tap
        scannerFragment.stopPreview();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if (scannerFragment != null && scannerFragment.isPreviewing()) {
            scannerFragment.stopPreview();
        }
    }

    @Override
    public boolean onTouchEvent (MotionEvent e)
    {
        if (gestureDetector != null)
            return gestureDetector.onTouchEvent (e);

        return super.onTouchEvent(e);
    }


    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed (MotionEvent e)
        {
            Log.d(TAG, "Single Tap Confirmed.");
            if (scannerFragment == null || scannerFragment.isPreviewing())
                return false;

            scannerFragment.isPreviewing();
            //setLabel(getString(R.string.scanning_label));
            Log.d(TAG, "Ok, scanning now.");

            return true;
        }
    }
}
