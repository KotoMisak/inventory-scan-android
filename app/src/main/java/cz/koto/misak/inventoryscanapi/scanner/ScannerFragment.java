package cz.koto.misak.inventoryscanapi.scanner;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.io.IOException;

import cz.koto.misak.inventoryscanapi.R;

public class ScannerFragment extends Fragment {

    private static String tag = ScannerFragment.class.getName();
    private static final int AUTOFOCUS_DELAY = 1000; //1s

    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler = null;

//    private Button scanButton;
    private ImageScanner scanner;

    private boolean barcodeScanned = false;
    private boolean previewing = true;

    private boolean initialized = false;
    private FloatingActionButton fabRestart;
    private FloatingActionButton fabAdd;

    private String lastScan;

    public ScannerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * assign variables, get Intent extras, and anything else that
         * doesn't involve the View hierarchy (i.e. non-graphical initialisations).
         */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /**
         * assign your View variables and do any graphical initialisations
         */
        View ret = inflater.inflate(R.layout.fragment_scanner, container, false);
        initControls(ret);
        return ret;

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    public final void initControls(View view) {
        initialized = true;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();

        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        scanner.setConfig(0, Config.ENABLE, 0); //Disable all the Symbols
        scanner.setConfig(Symbol.QRCODE, Config.ENABLE, 1); //Only QRCODE is enable


        mPreview = new CameraPreview(ScannerFragment.this.getActivity(), mCamera, previewCb,
                autoFocusCB);
        FrameLayout preview = (FrameLayout) view.findViewById(R.id.cameraPreview);
        preview.addView(mPreview);

//        scanButton = (Button) view.findViewById(R.id.ScanButton);
//
//        scanButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                if (barcodeScanned) {
//                    barcodeScanned = false;
//                    startPreview();
//                }
//            }
//        });

        fabRestart = (FloatingActionButton) view.findViewById(R.id.fab_restart);
        fabRestart.setVisibility(View.INVISIBLE);
        fabRestart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                barcodeScanned = false;
                startPreview();
            }
        });

        fabAdd = (FloatingActionButton) view.findViewById(R.id.fab_add);
        fabAdd.setVisibility(View.INVISIBLE);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.sendResult(lastScan);
                barcodeScanned = false;
                startPreview();
            }
        });

    }

    public void onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            releaseCamera();
        }
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    public final void releaseCamera() {
        if (mCamera != null) {
            stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing && mCamera!=null)
                mCamera.autoFocus(autoFocusCB);
        }
    };

    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                stopPreview();

                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {

                    Log.i("<<<<<<Asset Code>>>>> ",
                            "<<<<Bar Code>>> " + sym.getData());
                    lastScan = sym.getData().trim();


                    fabAdd.setVisibility(View.VISIBLE);
                    //TODO check if item already exists in list and offer ADD or UPDATE

//                    Toast.makeText(ScannerFragment.this.getActivity(), scanResult,
//                            Toast.LENGTH_SHORT).show();

                    barcodeScanned = true;

                    break;
                }
            }
        }
    };

    public void stopPreview() {
        try {
            if (mCamera == null) return;
            mCamera.setPreviewCallback(null);
            /**
             * Stop preview is used to be after autofocus, but this autofocus is
             * often failing and app needs to stop preview even so.
             */
            mCamera.stopPreview();
            mCamera.autoFocus(null);
        }catch (Throwable th){
            Log.e(getTag(),"stopPreview() Failed! "+th.getMessage());
        }finally {
            previewing = false;
            if (fabRestart!=null) fabRestart.setVisibility(View.VISIBLE);
        }
    }

    public boolean isPreviewing() {
        return previewing;
    }

    public void startPreview() {
        try {
            if (mCamera == null) return;
            mCamera.setPreviewCallback(previewCb);
            mCamera.startPreview();
            mCamera.autoFocus(autoFocusCB);
        }catch (Throwable th){
            Log.d(tag,"startPreview() Failed! "+th.getMessage());
        }finally {
            previewing = true;
            if (fabRestart!=null) fabRestart.setVisibility(View.INVISIBLE);
            if (fabAdd!=null) fabAdd.setVisibility(View.INVISIBLE);
        }
    }

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, AUTOFOCUS_DELAY);
        }
    };



    public static class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
        private SurfaceHolder mHolder;
        private Camera mCamera;
        private Camera.PreviewCallback previewCallback;
        private Camera.AutoFocusCallback autoFocusCallback;

        public CameraPreview(Context context, Camera camera,
                             Camera.PreviewCallback previewCb,
                             Camera.AutoFocusCallback autoFocusCb) {
            super(context);
            mCamera = camera;
            previewCallback = previewCb;
            autoFocusCallback = autoFocusCb;


            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {

            try {
                if (mCamera!=null) {
                    mCamera.setPreviewDisplay(holder);
                }
            } catch (IOException e) {
                Log.d("DBG", "Error setting camera preview: " + e.getMessage());
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
     /*
             * If your preview can change or rotate, take care of those events here.
             * Make sure to stop the preview before resizing or reformatting it.
             */
            if (mHolder.getSurface() == null) {
                // preview surface does not exist
                return;
            }

            // stop preview before making changes
            try {
                mCamera.stopPreview();
            } catch (Exception e) {
                // ignore: tried to stop a non-existent preview
            }

            try {
                // Hard code camera surface rotation 90 degs to match Activity view in portrait
                mCamera.setDisplayOrientation(90);

                mCamera.setPreviewDisplay(mHolder);
                mCamera.setPreviewCallback(previewCallback);
                mCamera.startPreview();
                mCamera.autoFocus(autoFocusCallback);
            } catch (Exception e) {
                Log.d("DBG", "Error starting camera preview: " + e.getMessage());
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }
    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
//                Log.d(tag, "Not visible anymore. Releasing camera.");
               stopPreview();//releaseCamera();
//                previewing = false;
            }else{
                startPreview();
            }
        }
    }


    /**
     * *****************************************************************
     */

    ScannResult mCallback;

    public interface ScannResult{
        public void sendResult(String text);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (ScannResult) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }
}
