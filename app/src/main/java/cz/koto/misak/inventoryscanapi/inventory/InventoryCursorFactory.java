package cz.koto.misak.inventoryscanapi.inventory;

import android.content.res.Resources;
import android.database.MatrixCursor;

import java.util.List;

import cz.koto.misak.inventoryscanapi.R;


public class InventoryCursorFactory {

    public static final String[] PROJECTION = new String[] { "_id", "itemImgStatus", "itemTxtStatus", "itemTxt" };


    public static MatrixCursor getCursor(Resources rsc) {
        MatrixCursor cData = new MatrixCursor(PROJECTION);
        List<InventoryItem> ret = InventoryMockFactory.mockInventoryItems();
        for (int i = 0; i < ret.size(); ++i) {
            InventoryItem ev = ret.get(i);
            int ico;
            String icoDesc;
            if (ev.getStatus() == null) {
                ico = R.drawable.ico_status_alert;
                icoDesc = rsc.getString(R.string.txt_available_alert);
            } else if (ev.getStatus().equals(Boolean.TRUE)) {
                ico = R.drawable.ico_status_ok;
                icoDesc = rsc.getString(R.string.txt_available_ok);
            } else /*if (ev.getStatus().equals(Boolean.FALSE))*/ {
                ico = R.drawable.ico_status_error;
                icoDesc = rsc.getString(R.string.txt_available_err);
            }


            cData.addRow(new Object[]{i, ico, icoDesc, ev.getText()});
        }
        return cData;
    }

    public static MatrixCursor getCursor(Resources rsc, InventoryItem item) {
        MatrixCursor cData = new MatrixCursor(PROJECTION);
            int ico;
            String icoDesc;
            if (item.getStatus() == null) {
                ico = R.drawable.ico_status_alert;
                icoDesc = rsc.getString(R.string.txt_available_alert);
            } else if (item.getStatus().equals(Boolean.TRUE)) {
                ico = R.drawable.ico_status_ok;
                icoDesc = rsc.getString(R.string.txt_available_ok);
            } else /*if (ev.getStatus().equals(Boolean.FALSE))*/ {
                ico = R.drawable.ico_status_error;
                icoDesc = rsc.getString(R.string.txt_available_err);
            }


            cData.addRow(new Object[]{0, ico, icoDesc, item.getText()});
        return cData;
    }
}
