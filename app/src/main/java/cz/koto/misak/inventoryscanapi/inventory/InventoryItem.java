package cz.koto.misak.inventoryscanapi.inventory;


public class InventoryItem {

    private Long id;
    private Boolean status;
    private String text;

    public InventoryItem() {
    }

    public InventoryItem(Long id, Boolean status, String text) {
        this.id = id;
        this.status = status;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
