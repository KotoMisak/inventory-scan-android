package cz.koto.misak.inventoryscanapi.inventory;


import java.util.ArrayList;
import java.util.List;

public class InventoryMockFactory {

    public static List<InventoryItem> mockInventoryItems(){
        List<InventoryItem> ret = new ArrayList<InventoryItem>();
        long id = 1;
//        ret.add(new InventoryItem(id++,Boolean.TRUE,"Fire engine"));
        ret.add(new InventoryItem(id++,null,"Laptop"));
//        ret.add(new InventoryItem(id++,Boolean.TRUE,"Fire house"));
//        ret.add(new InventoryItem(id++,Boolean.FALSE,"Fire truck"));
//        ret.add(new InventoryItem(id++,Boolean.TRUE, "Axe"));
//        ret.add(new InventoryItem(id++,Boolean.FALSE, "Shovel"));
//        ret.add(new InventoryItem(id++,null, "Mower"));
//        ret.add(new InventoryItem(id++,Boolean.TRUE, "Saw"));
        return ret;
    }

}
